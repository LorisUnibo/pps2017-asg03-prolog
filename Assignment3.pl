search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

% looks for two consecutive occurrences of Elem
search2(X,[X,X|_]).
search2(X,[_|Xs]):-search2(X,Xs).

% looks for two occurrences of Elem with an element in between!
search_two(X,[X,Y,X|_]):-X\=Y.
search_two(X,[_|Xs]):-search_two(X,Xs).

% looks for any Elem that occurs two times
search_anytwo(X,[X|Xs]):-search(X,Xs).
search_anytwo(X,[_|Xs]):-search_anytwo(X,Xs).

% Size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

% Size will contain the number of elements in List, 
% written using notation zero, s(zero), s(s(zero)).
sizeS([],zero).
sizeS([_|T],M) :- sizeS(T,N), M = s(N).

%sum
sum([],0).
sum([H|T],S) :- sum(T,N), S is N+H.

% average
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :- C2 is C+1, S2 is S+X, average(Xs,C2,S2,A).

% Max is the biggest element in List
% Suppose the list has at least one element
max([H|T],Max) :- max(T,Max,H).
max([],Max,TempMax) :- Max = TempMax.
max([H|T],Max,TempMax) :- H > TempMax, max(T,Max,H).
max([H|T],Max,TempMax) :- H =< TempMax, max(T,Max,TempMax).

% are the two lists the same?
same([],[]).
same([X|Xs],[X|Xs]).

% all elements in List1 are bigger than those in List2, 1 by 1
all_bigger([X|Xs],[Y|Ys]) :- X>Y, all_bigger(Xs,Ys).
all_bigger([X],[Y]) :- X>Y.

% List1 should be a subset of List2
sublist([],[H|_]).
sublist([X|Xs],List2) :- search(X,List2), sublist(Xs,List2).

%seq(4,[0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

% seqR(4,[4,3,2,1,0]).
seqR(0,[0]).
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

%seqR2(4,[0,1,2,3,4]).
seqR2(N,List) :- seqR2(N,0,List).
seqR2(N,N,[N]).
seqR2(N,A,[A|T]):- N >= A, A2 is A+1, seqR2(N,A2,T).

% inv([1,2,3],[3,2,1]).
inv([],[]).
inv([X|Xs],I):-inv(Xs,Y), append(Y,[X],I).

%double([1,2,3],[1,2,3,1,2,3]).
double(L,Y):-append(L,L,Y).

%times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(L,1,L).
times(L,N,Y) :- N>1, N2 is N-1, times(L,N2,Y2), append(Y2, L, Y).

% proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([],[]).
proj([[H|_]|T],[H|Y]):-proj(T,Y).

%Drops any occurrence of element
%dropAny(Elem,List,OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).
%drops only the first occurrence
dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):-dropAny(X,Xs,L),!.
%drops only the last occurrence
dropLast(E,List,Out):-
	inv(List,Inv), 
	dropFirst(E, Inv, OutInv), 
	inv(OutInv,Out).
%drop all occurrences, returning a single list as result
dropAll(X,L,O):-search(X,L),dropFirst(X,L,O2),dropAll(X,O2,O),!.
dropAll(X,L,L):- \+ search(X,L).

%obtains a graph from a list
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

%obtains a circular graph from a list
%fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
fromCircList([_],[]):-!.
fromCircList([H|T],G):- 
	fromList([H|T],G2),
	inv([H|T],[H2|_]),
	append(G2,[e(H2,H)],G).

% drop all edges starting and leaving from a Node
dropNode(G,N,O):- dropAll(e(N,_),G,G2),dropAll(e(_,N),G2,O).

% all the nodes that can be reached in 1 step from Node
%reaching([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2,3]
reaching([],_,[]).
reaching([e(E,H2)|T],E,O):-reaching(T,E,O2),append(O2,[H2],O),!.
reaching([e(H1,E)|T],E,O):-reaching(T,E,O2),append(O2,[H1],O),!.
reaching([_|T],E,O):-reaching(T,E,O).

% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath(G,N1,N2,O):-search(e(N1,N2),G),O=[e(N1,N2)].
anypath(G,N1,N2,O):-
	search(e(N1,N3),G),
	anypath(G,N3,N2,O2),
	append([e(N1,N3)],O2,O).

% all the nodes that can be reached from Node
allreaching(G,E,O):-findall(E2,anypath(G,E,E2,_),O).