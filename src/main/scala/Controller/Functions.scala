package Controller

import Model.Edge

trait Functions {
  /**
    * Checks if given element is contained in the list
    * @param list the list to use
    * @param element the element to be searched
    * @tparam T the type of the list elements
    * @return true if element is contained in list, false otherwise
    */
  def Search[T](list: List[T], element: T):Boolean
  /**
    * Checks if given list contains two consecutive occurrences of element
    * @param list the list to use
    * @param element the element to be searched
    * @tparam T the type of the list elements
    * @return true if element occurs 2 consecutive times in list, false otherwise
    */
  def Search2[T](list: List[T], element: T):Boolean
  /**
    * Checks if given list contains two occurrences of element with an element in between
    * @param list the list to use
    * @param element the element to be searched
    * @tparam T the type of the list elements
    * @return true if element occurs 2 times in list with an element in between, false otherwise
    */
  def Search_two[T](list: List[T], element: T):Boolean
  /**
    * Checks if given list contains two occurrences of element
    * @param list the list to use
    * @param element the element to be searched
    * @tparam T the type of the list elements
    * @return true if element occurs 2 times in list, false otherwise
    */
  def Search_anytwo[T](list: List[T], element: T):Boolean
  /**
    * Gives the size of list
    * @param list the list to use
    * @tparam T the type of the list elements
    * @return the size of list
    */
  def Size[T](list: List[T]):Int
  /**
    * Gives the size of list using successors notation
    * @param list the list to use
    * @tparam T the type of the list elements
    * @return the size of list in successor notation
    */
  def SizeS[T](list: List[T]):String
  /**
    * Gives the sum of the elements
    * @param list the list to use
    * @return the sum of the elements
    */
  def Sum[T : Numeric](list: List[T]):T
  /**
    * Gives the average of the elements
    * @param list the list to use
    * @return the average of the elements
    */
  def Average[T : Numeric](list: List[T]):Double
  /**
    * Gives the max of the elements
    * @param list the list to use
    * @return the max of the elements
    */
  def Max[T : Numeric](list: List[T]):T
  /**
    * Checks if 2 lists contains the same elements
    * @param list1 the first list to use
    * @param list2 the second list to use
    * @tparam T the type of the list elements
    * @return true if the given lists contains the same elements, false otherwise
    */
  def Same[T](list1: List[T], list2: List[T]):Boolean
  /**
    * Checks if the elements of the first list are all bigger than the ones in the 2nd one
    * @param list1 the first list to use
    * @param list2 the second list to use
    * @return true if the elements of the 1st list are all bigger than the ones in the 2nd list, false otherwise.
    */
  def All_Bigger[T : Numeric](list1: List[T], list2: List[T]):Boolean
  /**
    * Checks if the set is a subset of the list
    * @param set1 the subset to check
    * @param list the list to use
    * @tparam T the type of the list elements
    * @return true is the set is a subset of list, false otherwise
    */
  def Sublist[T](set1: Set[T], list: Set[T]): Boolean
  /**
    * Generates a sequence of zeroes
    * @param count the wanted length of the sequence
    * @return the generated sequence
    */
  def Seq(count: Int): Seq[Int]
  /**
    * Generates a decreasing sequence from count to 0
    * @param count where to start the sequence
    * @return the generated sequence
    */
  def SeqR(count: Int): Seq[Int]
  /**
    * Generates an increasing sequence from 0 to count
    * @param count where to end the sequence
    * @return the generated sequence
    */
  def SeqR2(count: Int): Seq[Int]
  /**
    * Reverts the list
    * @param list the list to use
    * @tparam T the type of the list elements
    * @return the reversed list
    */
  def Inv[T](list: List[T]): Seq[T]
  /**
    * Creates a list attaching a copy of its elements at its end
    * @param list the list to use
    * @tparam T the type of the list elements
    * @return the doubled list
    */
  def Double[T](list: List[T]): Seq[T]
  /**
    * Creates a list with count copies of its elements
    * @param list the list to use
    * @param count the wanted number of replications
    * @tparam T the type of the list elements
    * @return the generated list
    */
  def Times[T](list: List[T], count: Int): Seq[T]
  /**
    * Creates a projection using the first elements of list's ones.
    * @param list the list to use
    * @tparam T the type of the list elements
    * @return the projection list
    */
  def Proj[T](list: List[List[T]]): Seq[T]
  /**
    * Deletes any occurrence of element from list
    * @param list the list to use
    * @param element the element to delete
    * @tparam T the type of the list elements
    * @return all the possible lists without one element
    */
  def DropAny[T](list: List[T], element: T): List[Seq[T]]
  /**
    * Deletes the first occurrence of element from list
    * @param list the list to use
    * @param element the element to delete
    * @tparam T the type of the list elements
    * @return the list without the first occurrence of element
    */
  def DropFirst[T](list: List[T], element: T): Seq[T]
  /**
    * Deletes the last occurrence of element from list
    * @param list the list to use
    * @param element the element to delete
    * @tparam T the type of the list elements
    * @return the list without the last occurrence of element
    */
  def DropLast[T](list: List[T], element: T): Seq[T]
  /**
    * Deletes all the occurrences of element from list
    * @param list the list to use
    * @param element the element to delete
    * @tparam T the type of the list elements
    * @return the list without all the occurrences of element
    */
  def DropAll[T](list: List[T], element: T): Seq[T]

  type Graph[T] = Set[Edge[T]]
  type Path[T] = List[Edge[T]]

  /**
    * Creates a Graph from a list of nodes, creating edges between couples following their order
    * @param list the list of nodes
    * @tparam T the node type
    * @return the generated graph
    */
  def FromList[T](list: List[T]): Graph[T]
  /**
    * Creates a Graph from a list of nodes, creating edges between couples following their order.
    * Also connects the last node with the first one.
    * @param list the list of nodes
    * @tparam T the node type
    * @return the generated graph
    */
  def FromCircList[T](list: List[T]): Graph[T]
  /**
    * Drop all edges starting and leaving from a Node
    * @param graph the graph to use
    * @param element the element to drop
    * @tparam T the node type
    * @return The graph without the given node
    */
  def DropNode[T](graph: Graph[T], element: T): Graph[T]
  /**
    * Finds all the nodes that can be reached in 1 step from a given one
    * @param graph the graph to use
    * @param startingNode the starting node
    * @tparam T the node type
    * @return The list of reachable nodes
    */
  def Reaching[T](graph: Graph[T], startingNode: T): Seq[T]
  /**
    * Finds all possible paths between two nodes in an acyclic graph
    * @param graph the graph to use
    * @param fromNode the starting node
    * @param toNode the target node
    * @tparam T the node type
    * @return The possible paths from fromNode to toNode.
    */
  def AnyPath[T](graph: Graph[T], fromNode: T, toNode: T): Seq[Path[T]]
  /**
    * Finds all the nodes that can be reached from a given one in an acyclic graph
    * @param graph the graph to use
    * @param node the starting node
    * @tparam T the node type
    * @return The list of reachable nodes
    */
  def AllReaching[T](graph: Graph[T], node: T): Seq[T]
}
