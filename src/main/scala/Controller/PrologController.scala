package Controller

import Model.{Edge, PrologEngine}
import alice.tuprolog.Term

object PrologController extends Functions {
  implicit def stringToTerm(s: String): Term = Term.createTerm(s)
  implicit def TraversableToTerm[T](collection: Traversable[T]): Term = collection.head match {
    case _: Traversable[_] =>
      collection.map(item => TraversableToTerm(item.asInstanceOf[Traversable[_]])).mkString("[", ",", "]")
    case _ => collection.mkString("[", ",", "]")
  }
  implicit def TermToSeq[T](term: Term): Seq[T] =
    term.toString.drop(1).dropRight(1).split(',').map(_.asInstanceOf[T])
  implicit def TermToGraph[T](term: Term): Graph[T] = TermToPath[T](term).toSet
  implicit def TermToPath[T](term: Term): Path[T] = {
    term.toString.drop(1).dropRight(1)
      .replace(",e"," e")
      .split(' ')
      .map(Edge[T]).toList
  }

  override def Search[T](list: List[T], element: T): Boolean =
    PrologEngine.Solve("search", element.toString, list).isSuccess

  override def Search2[T](list: List[T], element: T): Boolean =
    PrologEngine.Solve("search2", element.toString, list).isSuccess

  override def Search_two[T](list: List[T], element: T): Boolean =
    PrologEngine.Solve("search_two", element.toString, list).isSuccess

  override def Search_anytwo[T](list: List[T], element: T): Boolean =
    PrologEngine.Solve("search_anytwo", element.toString, list).isSuccess

  override def Size[T](list: List[T]): Int =
    PrologEngine.Solve("size", list, "N").getVarValue("N").toString.toInt

  override def SizeS[T](list: List[T]): String =
    PrologEngine.Solve("sizeS", list, "N").getVarValue("N").toString

  override def Sum[T : Numeric](list: List[T]): T =
    PrologEngine.Solve("sum", list, "N").getVarValue("N").toString.asInstanceOf[T]

  override def Average[T : Numeric](list: List[T]): Double =
    PrologEngine.Solve("average", list, "N").getVarValue("N").toString.toDouble

  override def Max[T : Numeric](list: List[T]): T =
    PrologEngine.Solve("max", list, "N").getVarValue("N").toString.asInstanceOf[T]

  override def Same[T](list1: List[T], list2: List[T]): Boolean =
    PrologEngine.Solve("same", list1, list2).isSuccess

  override def All_Bigger[T : Numeric](list1: List[T], list2: List[T]): Boolean =
    PrologEngine.Solve("all_bigger", list1, list2).isSuccess

  override def Sublist[T](set1: Set[T], list: Set[T]): Boolean =
    PrologEngine.Solve("sublist", set1, list).isSuccess

  override def Seq(count: Int): Seq[Int] =
    PrologEngine.Solve("seq", count.toString, "L").getVarValue("L")

  override def SeqR(count: Int): Seq[Int] =
    PrologEngine.Solve("seqR", count.toString, "L").getVarValue("L")

  override def SeqR2(count: Int): Seq[Int] =
    PrologEngine.Solve("seqR2", count.toString, "L").getVarValue("L")

  override def Inv[T](list: List[T]): Seq[T] =
    PrologEngine.Solve("inv", list, "L").getVarValue("L")

  override def Double[T](list: List[T]): Seq[T] =
    PrologEngine.Solve("double", list, "L").getVarValue("L")

  override def Times[T](list: List[T], count: Int): Seq[T] =
    PrologEngine.Solve("times", list, count.toString, "L").getVarValue("L")

  override def Proj[T](list: List[List[T]]): Seq[T] =
    PrologEngine.Solve("proj", list, "L").getVarValue("L")

  override def DropAny[T](list: List[T], element: T): List[Seq[T]] =
    PrologEngine.SolveAll("dropAny", element.toString, list, "L")
      .map(solution => {
        val result: Seq[T] = solution.getVarValue("L")
        result
      })

  override def DropFirst[T](list: List[T], element: T): Seq[T] =
    PrologEngine.Solve("dropFirst", element.toString, list, "L").getVarValue("L")

  override def DropLast[T](list: List[T], element: T): Seq[T] =
    PrologEngine.Solve("dropLast", element.toString, list, "L").getVarValue("L")

  override def DropAll[T](list: List[T], element: T): Seq[T] =
    PrologEngine.Solve("dropAll", element.toString, list, "L").getVarValue("L")

  override def FromList[T](list: List[T]): Graph[T] =
    PrologEngine.Solve("fromList", list, "G").getVarValue("G")

  override def FromCircList[T](list: List[T]): Graph[T] =
    PrologEngine.Solve("fromCircList", list, "G").getVarValue("G")

  override def DropNode[T](graph: Graph[T], element: T): Graph[T] =
    PrologEngine.Solve("dropNode", graph, element.toString, "G").getVarValue("G")

  override def Reaching[T](graph: Graph[T], startingNode: T): Seq[T] =
    PrologEngine.Solve("reaching", graph, startingNode.toString, "L").getVarValue("L")

  override def AnyPath[T](graph: Graph[T], fromNode: T, toNode: T): List[Path[T]] =
    PrologEngine.SolveAll("anypath", graph, fromNode.toString, toNode.toString, "P")
      .map(solution => {
        val path: Path[T] = solution.getVarValue("P")
        path
      })

  override def AllReaching[T](graph: Graph[T], node: T): Seq[T] =
    PrologEngine.Solve("allreaching", graph, node.toString, "L").getVarValue("L")
}
