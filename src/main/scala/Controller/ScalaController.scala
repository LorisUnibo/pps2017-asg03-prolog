package Controller

import Model.Edge

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object ScalaController extends Functions {
  override def Search[T](list: List[T], element: T): Boolean = list.contains(element)

  override def Search2[T](list: List[T], element: T): Boolean =
    list.sliding(2).exists(e => e.head.equals(element) && e(1).equals(element))

  override def Search_two[T](list: List[T], element: T): Boolean =
    list.sliding(3).exists(e => e.head.equals(element) && e(2).equals(element) && !e(1).equals(element))

  override def Search_anytwo[T](list: List[T], element: T): Boolean = list.diff(list.distinct).contains(element)

  override def Size[T](list: List[T]): Int = list.size

  override def SizeS[T](list: List[T]): String = {
    def writeS(count: Int): String = count match {
      case 0 => "zero"
      case _ => "s(" + writeS(count - 1) + ")"
    }
    writeS(list.size)
  }

  override def Sum[T : Numeric](list: List[T]): T = list.sum

  override def Average[T : Numeric](list: List[T]): Double = list.sum.asInstanceOf[Double] / list.size

  override def Max[T : Numeric](list: List[T]): T = list.max

  override def Same[T](list1: List[T], list2: List[T]): Boolean = list1 == list2

  import Ordering.Implicits._
  override def All_Bigger[T : Numeric](list1: List[T], list2: List[T]): Boolean =
    list1.zip(list2).forall { case (e1, e2) => e1 > e2 }

  override def Sublist[T](set1: Set[T], list: Set[T]): Boolean = set1.subsetOf(list)

  override def Seq(count: Int): Seq[Int] = scala.collection.Seq.fill(count)(0)

  override def SeqR(count: Int): Seq[Int] = scala.collection.Seq.range(count,-1)

  override def SeqR2(count: Int): Seq[Int] = scala.collection.Seq.range(0,count+1)

  override def Inv[T](list: List[T]): List[T] = list.reverse

  override def Double[T](list: List[T]): List[T] = list.union(list)

  override def Times[T](list: List[T], count: Int): List[T] = count match {
    case 0 => List.empty[T]
    case _ if count > 0 =>
      val result = List.empty[T]
      (1 to count).foreach(_ => result.union(list))
      result
  }

  override def Proj[T](list: List[List[T]]): List[T] = list.map(_.head)

  private def ConcatParts[T](parts: (List[T], List[T])): List[T] = parts._1 ::: parts._2.tail

  override def DropAny[T](list: List[T], element: T): List[List[T]] = {
    var result = scala.collection.mutable.ListBuffer.empty[List[T]]
    (0 to list.size).foreach(i =>
      if (list(i) == element) result += ConcatParts(list.splitAt(i))
    )
    result.toList
  }

  override def DropFirst[T](list: List[T], element: T): List[T] = DropLast(list.reverse, element).reverse

  //span splits list using the biggest before as possible
  override def DropLast[T](list: List[T], element: T): List[T] = ConcatParts(list.span(_ == element))

  override def DropAll[T](list: List[T], element: T): List[T] = list.filter(_ != element)


  override def FromList[T](list: List[T]): Graph[T] = list.sliding(2).map(group => Edge(group.head,group(1))).toSet

  override def FromCircList[T](list: List[T]): Graph[T] = FromList(list) + Edge(list.last,list.head)

  override def DropNode[T](graph: Graph[T], element: T): Graph[T] = graph.filter(!_.HasNode(element))

  override def Reaching[T](graph: Graph[T], startingNode: T): List[T] = graph.toList.filter(_.start == startingNode).map(_.end)

  override def AnyPath[T](graph: Graph[T], fromNode: T, toNode: T): List[Path[T]] = fromNode match {
    case _ if fromNode == toNode => List.empty[Path[T]]
    case _ =>
      val paths = Reaching(graph, fromNode)
        .filterNot(_ == toNode)
        .flatMap(AnyPath(graph, _, toNode))
        .map(path => Edge(fromNode, path.head.start) :: path)

      if (graph.contains(Edge(fromNode, toNode)))
        List(Edge(fromNode, toNode)) :: paths
      else
        paths
  }

  override def AllReaching[T](graph: Graph[T], node: T): List[T] = {
    val reaching = Reaching(graph, node)
    reaching ::: reaching.flatMap(AllReaching(graph, _))
  }
}
