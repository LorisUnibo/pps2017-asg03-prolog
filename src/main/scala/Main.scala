import Controller.{PrologController, ScalaController}
import Model.{Edge, Graph}

object Main extends App {
  val prologResult = PrologController.AnyPath(Graph((1,2),(1,3),(2,3)), 1,3)
  val scalaResult = ScalaController.AnyPath(Graph((1,2), (2,3), (1,3)), 1,3)

  val prologCheck = PrologController.Same(prologResult, scalaResult)
  val scalaCheck = ScalaController.Same(prologResult, scalaResult)
  println(prologCheck && scalaCheck && prologCheck==scalaCheck)
}