package Model

object Edge {
  def apply[T](node1: T, node2: T): Edge[T] = new Edge[T](node1, node2)
  def apply[T](stringRepresentation: String): Edge[T] = stringRepresentation match {
    case "" => null
    case _ =>
      //println(stringRepresentation)
      val parts = stringRepresentation.drop(2).dropRight(1).split(',')
      apply(parts(0).asInstanceOf[T], parts(1).asInstanceOf[T])
  }

  //def unapply[T](edge: Edge[T]): Option[(T, T)] = Option((edge.start, edge.end))
}

class Edge[T] (val start: T, val end: T) {
  def HasNode(node: T): Boolean = start == node || end == node

  override def toString: String = "e(" + start.toString + "," + end.toString + ")"

  override def equals(obj: Any): Boolean = obj match {
    case edge: Edge[T] => edge.toString.equals(toString)
    case _ => super.equals(obj)
  }
}
