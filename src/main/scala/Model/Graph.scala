package Model

object Graph {
  def apply[T](nodes: (T,T)*): Set[Edge[T]] = nodes.map {
    case (node1, node2) => Edge(node1, node2)
  }.toSet
}
