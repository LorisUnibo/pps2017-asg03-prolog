package Model

import java.io.FileInputStream

import alice.tuprolog.{NoMoreSolutionException, _}

object PrologEngine {
  val engine = new Prolog
  engine.setTheory(new Theory(new  FileInputStream ("Assignment3.pl")))

  implicit def stringToTerm(s: String): Term = Term.createTerm(s)
  implicit def TraversableToTerm[T](collection: Traversable[T]): Term = collection.mkString("[",",","]")

  def Solve(function: String, terms: Term*):SolveInfo = Solve(s"$function(${terms.mkString(",")}).")
  def Solve(goal: String):SolveInfo = engine.solve(goal)

  def SolveAll(function: String, terms: Term*):List[SolveInfo] = SolveAll(s"$function(${terms.mkString(",")}).")
  def SolveAll(goal: String): List[SolveInfo] = {
    var results = scala.collection.mutable.ListBuffer.empty[SolveInfo]
    var solution = Solve(goal)
    results += solution
    while (solution.isSuccess || solution.hasOpenAlternatives) {
      solution = engine.solveNext()
      if (solution.isSuccess)
        results += solution
    }
    results.toList
  }
}
